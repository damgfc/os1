/*
$Author: o-moring $
$Log: mydu.c,v $
Revision 1.2  2015/02/10 08:16:42  o-moring
final revision

Revision 1.1  2015/01/30 03:58:26  o-moring
Initial revision

$Date: 2015/02/10 08:16:42 $
*/

/**************/
/* o-moring.1 */
/**************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/queue.h>
#include <dirent.h>
#include <limits.h>
#include <getopt.h>
#include <string.h>

//Initialize the queue
TAILQ_HEAD(tailhead, entry) head;
struct tailhead *headp;
struct entry {
	char path[PATH_MAX];
	TAILQ_ENTRY(entry) entries;
} *tailp, *newNode;

/*Global Variables  *
 * Option Flags and *
 *  Program Name    */
int aFlag = 0;
int sFlag = 0;
int hFlag = 0;
int H_Flag = 0;
int kFlag = 0;
int LFlag = 0;
int xFlag = 0;

int sizepathfun(char *path){
	//printf("pathfun1: %s\n", path);
	struct stat buffer;
	if (stat(path, &buffer) == 0){
		return buffer.st_size;
	}
	else {
		char output[60]; 
		sprintf(output, "cannot mydu `%s'", path);
		perror(output);
		return -1;	
	}
}

int breadthfirstapply(char * path, int *pathfun( char *path1)) {
	//Queue Variables
	TAILQ_INIT(&head);
	headp = &head;
	tailp = (struct entry*) malloc (sizeof(struct entry));
	
	//Directory Variables
	DIR *directoryp;
	struct dirent *direntp;
	struct stat buffer;
	int directorySize, size, sum;
	sum = 0;
	char fullpath[PATH_MAX];
	char currentDirectory[PATH_MAX];
	
		
	newNode = (struct entry*) malloc(sizeof(struct entry));

	//Enqueue root directory
	strncpy(newNode->path, path, PATH_MAX);
	TAILQ_INSERT_TAIL(headp, newNode, entries);
	
	//While queue is not empty
	while(!TAILQ_EMPTY(headp)){
		//Dequeue next
		newNode = headp->tqh_first;
		strncpy(currentDirectory, newNode->path, PATH_MAX);

		TAILQ_REMOVE(headp, newNode, entries);
		free(newNode);
		if ((directoryp = opendir(currentDirectory)) == NULL){
			TAILQ_REMOVE(headp, newNode, entries);
		}
		
		size = 0;
		
		//Read all files in directory.
		while (directoryp != NULL && (direntp = readdir(directoryp))){
			snprintf(fullpath, PATH_MAX, "%s/%s", currentDirectory, direntp->d_name);
			lstat(fullpath, &buffer);
			
			if (S_ISDIR(buffer.st_mode)) {
				if (aFlag) {
					printf("Directory: %s\n", fullpath);
				}
				
				// if the directory name isn't . or ..
				if(strcmp(direntp->d_name, ".") && strcmp(direntp->d_name, "..")){
					newNode = (struct entry *) malloc(sizeof(struct entry));
					strncpy(newNode->path, fullpath, PATH_MAX);
					TAILQ_INSERT_TAIL(&head, newNode, entries);
				}
			}
			else if ((buffer.st_mode & S_IFMT) == S_IFREG) {
				if (aFlag) {
					printf("Regular File: %s\n", fullpath);
				}
			}			
			else if ((buffer.st_mode & S_IFMT) == S_IFLNK) {
				if (aFlag) {
					printf("Regular File: %s\n", fullpath);
				}			
			}
			else if ((buffer.st_mode & S_IFMT) == S_IFIFO) {
				if (aFlag) {
					printf("IFO: %s\n", fullpath);
				}			
			}			
			else if ((buffer.st_mode & S_IFMT) == S_IFBLK) {
				if (aFlag) {
					printf("Block: %s\n", fullpath);
				}			
			}
			else if ((buffer.st_mode & S_IFMT) == S_IFCHR) {
				if (aFlag) {
					printf("Character: %s\n", fullpath);
				}			
			}			
			else if ((buffer.st_mode & S_IFMT) == S_IFSOCK) {
				if (aFlag) {
					printf("Socket: %s\n", fullpath);
				}			
			}
			else{
				if (aFlag){
					printf("Unknown: %s\n", fullpath);
				}
			}
		}
		size = buffer.st_size;
		closedir(directoryp);
		sum = sum + size;
		

	}
	if (aFlag && sFlag){
		char *errorCode;
		snprintf(errorCode, PATH_MAX, "-a and -s flags not allowed together");
		perror(errorCode);
		exit(1);
	}
	else if (sFlag) {
		printf("%d %s\n", sum/256, path);
	}
}

int main(int argc, char **argv){
	int option;

	while ((option = getopt (argc, argv, "ashHkLx")) != -1){  
		switch(option){
			case 'a':
				aFlag = 1;	
				break;
			case 's':
				sFlag = 1;
				break;
			case 'h':
				hFlag = 1;
				break;
			case 'H':
				H_Flag = 1;
				break;
			case 'k':
				kFlag = 1;
				break;
			case 'L':
				LFlag = 1;
				break;				
			case 'x':
				xFlag = 1;
				break;				
			case '?':
				perror("Unknown option received");
				return -1;	
			default:
				abort();
		}
	}

	breadthfirstapply(argv[optind], sizepathfun);
		
}
