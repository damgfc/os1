CC = gcc
CFLAGS = 
OBJS = mydu.o 
PROG = mydu

.SUFFIXES: .c .o

$(PROG): $(OBJS)
	$(CC) -g -o $@ $(OBJS)

.c.o:
	$(CC) -c -o $@ $<
	
clean:
	rm *.o $(PROG)
